#!/usr/bin/bash

_buildsh_path="$(realpath -- "$0")"

exec ./mkfirerainiso "$@" "${_buildsh_path%/*}"
